FROM debian
RUN apt-get update && apt-get install git -y
COPY sync-with-repo.sh /
ENTRYPOINT ["/sync-with-repo.sh"]
