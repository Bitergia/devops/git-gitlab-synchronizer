#!/bin/bash

function check_variables
    if [ -z "$TARGET_DIR" ]; then
    echo "Missing env variable TARGET_DIR"
    exit 1
    fi

    if [ -z "$CREDENTIAL" ]; then
    echo "Missing env variable CREDENTIAL"
    exit 1
    fi

    if [ -z "$GITLAB_REPO" ]; then
    echo "Missing env variable GITLAB_REPO"
    exit 1
    fi

    if [ -z "$FREQ" ]; then
    echo "Missing env variable FREQ"
    exit 1
    fi

# main starts here

check_variables

while true
do

    if [ -d "$TARGET_DIR" ]; then
        cd $TARGET_DIR
        if [ ! -d ".git" ]; then
            git init
            git remote add origin https://$CREDENTIAL@gitlab.com/$GITLAB_REPO
        fi
        git pull origin master
        cd ..
    else
        git clone https://$CREDENTIAL@gitlab.com/$GITLAB_REPO $TARGET_DIR
    fi

sleep $FREQ

done
