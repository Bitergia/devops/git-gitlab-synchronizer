# git-gitlab-synchronizer

Demo microservice that keeps a local copy updated of a remote Gitlab repo

# Example of use

```
version: '3'

services:
  web:
    image: gitlab-repo-sync:latest
    environment:
      - CREDENTIAL=sanacl:sietec4b4llosvienendebon4nz4
      - GITLAB_REPO=Bitergia/myrepo.git
      - FREQ=3
      - TARGET_DIR=/repos/setup
    volumes:
      - /tmp:/repos
```

The environment variables here are:
* CREDENTIAL: user name + token
* GITLAB_REPO: the part of the URL after https://gitlab.com/
* FREQ: number of seconds
* TARGET_DIR: where the repository directory will be created
